/**
 * Configuration values
 */

const Config = {
    mapList : {
        initialPoint: [21.505, -0.09],
        initialZoom : 2,
        userZoom: 11,
        accessToken: 'pk.eyJ1IjoieGFuZGVzaW1vZXMiLCJhIjoiY2o1anNsbnQxMmJ0eDMzb2R1bjkxaWJmMSJ9.IFadPg3x8wg1BxhoAGSKuA'
    }
};

export default Config;