/**
 * Define a single (and simple) page with a icon and message
 */
import React from 'react';

import './IconTextPage.css';

function IconTextPage(props){
    return (
        <div className="IconTextPage">
            {props.iconClass && <i className={props.iconClass}></i>}
            {props.msg}
    </div>);
}


export default IconTextPage;