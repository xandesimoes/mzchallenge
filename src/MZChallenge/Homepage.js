/**
 * Homepage contains the Users Card List and the big map with all users
 */
import React from 'react';
import UserCardList from '../user/UserCardList';
import UsersMap from './containers/UsersMap';

function Homepage(props) {
    return (
      <div className="Homepage">
            <UserCardList users={props.users} selectedUserCallback={props.selectedUserCallback} />
            <UsersMap users={props.users} selectedUserIdx={props.selectedUserIdx} />
        </div>
    );
}

export default Homepage;
