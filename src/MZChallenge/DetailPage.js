/**
 * Process Current User Detail
 *
 * fetch the index from route param and
 * select the user from users array in props
 */
import React, { Component } from 'react';
import UserDetail from '../user/UserDetail';
import IconTextPage from '../content/IconTextPage';

class DetailPage extends Component {
  render() {
      let index = this.props.routeinfo.match.params.index;

      let user = this.props.users[index];

      //if user does not exist, the index is invalid
      if(!user) return <IconTextPage iconClass="fa fa-exclamation" msg={"Could not load info for the index "+index} />;

    return <UserDetail user={user} />
  }
}

export default DetailPage;
