/**
 * Map with users info
 * 
 * it shows a big map if has prop.users (Array)
 * or a detailed Map if has prop.user (Object)
 */
import React, { Component } from 'react';
import Config from '../../Config';
import MapInstance from '../../map/MapInstance';

class UsersMap extends Component {

  //convert user info to point info
  userToPoint(user){
    let coords = user.address.geo;
    return {
      lat : coords.lat,
      lng : coords.lng,
      popupContent: this.popupContent(user)
    };
  }

  //get html to popup content
  popupContent(user){
    let coords = user.address.geo;
    return '<h5>'+user.name+'</h5><a href="'+user.website+'">'+user.website+'</a><br/><br/>('+coords.lat+','+coords.lng+')';
  }

  render(){
    //define initial values for the map
    let initialCoord = this.props.users ? Config.mapList.initialPoint : [this.props.user.address.geo.lat, this.props.user.address.geo.lng];
    let initialZoom = this.props.users ? Config.mapList.initialZoom : Config.mapList.userZoom;

    //get the points for the map - if has only one user pass a array with that user
    let points = this.props.users ?
              this.props.users.map( (user) => this.userToPoint(user) ) :
              [this.userToPoint(this.props.user) ];
    //style specific - if is in homepage or is detailed
    let className = this.props.users ? 'col-xs-8 col-sm-9 mapcontainer-allusers' :'mapcontainer-oneuser col-xs-5 col-sm-8';


    return <MapInstance className={className} points={points} initialCoord={initialCoord} initialZoom={initialZoom} selectedPoint={this.props.selectedUserIdx} />;
  }
}

export default UsersMap;
