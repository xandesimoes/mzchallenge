/**
 * APP Main container
 * 
 * Defines the Routes and has the main state of the app
 * also makes the user API request
 */

import React, { Component } from 'react';
import axios from 'axios';

import './App.css';
import Homepage from './Homepage';
import DetailPage from './DetailPage';

import {Route, Link, Switch} from 'react-router-dom';

import IconTextPage from '../content/IconTextPage';

class App extends Component {

    constructor(props){
      super(props);

      this.state = {
        loading:true,
        error: false,
        selectedUserIdx: false,
        users: []
      }

      this.selectedUserCallback = this.selectedUserCallback.bind(this);

    }

  //load Users from API
  componentDidMount(){
    axios.get('https://jsonplaceholder.typicode.com/users')
      .then( (response) => {
        this.setState(
          {
            loading:false,
            users: response.data
          }
        );
      })
      .catch( (error) => {
        this.setState(
          {
            loading:false,
            error:true
          }
        );
      });
  }

  selectedUserCallback(idx){
    this.setState({selectedUserIdx:idx});
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2><Link to="/">MZChallenge</Link></h2>
        </div>

        {this.state.loading && <IconTextPage iconClass="fa fa-spinner fa-spin fa-3x fa-fw" msg="Loading..." />}

        {!this.state.loading && !this.state.error && (
            <div className="App-content">
              <Switch>
                <Route exact path="/" render={ () => <Homepage users={this.state.users} selectedUserIdx={this.state.selectedUserIdx} selectedUserCallback={this.selectedUserCallback} />} /> 
                <Route path="/detail/:index" render={( routeinfo ) => <DetailPage users={this.state.users} routeinfo={routeinfo}/> } />
                <Route path="*" render={() => <IconTextPage iconClass="fa fa-question" msg="Path not found" /> } />
              </Switch>
            </div>
        )}

          {this.state.error && <IconTextPage iconClass="fa fa-exclamation" msg="Could not load info" />}
      </div>
    );
  }
}

export default App;
