/**
 * User Card List
 * props:
 *  selectedUserCallback : function(index)  - to update the active homepage user
 */
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import UserCard from './UserCard';
import './UserCardList.css';

class UserCardList extends Component{
    constructor(props){
        super(props);

        this.hoverUser = this.hoverUser.bind(this);
    }

    /**
     * 
     * on mouse hover update the parent info
     * [Int ]index - of the selected element
     */
    hoverUser(index){
        this.props.selectedUserCallback(index);
    }

    render(){
        return (
            <div className="col-xs-4 col-sm-3 list-group cardsContainer">
                {this.props.users.map((user, index) => {
                return  (
                        <Link key={index} to={"/detail/"+index} className="list-group-item"  onMouseOver={() =>this.hoverUser(index)}>
                            <UserCard user={user}/>
                        </Link>
                    )
                })}
            </div>
        )
    }
}

export default UserCardList;
