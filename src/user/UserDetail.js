/**
 * Detailed Info for the user
 */
import React from 'react';
import UsersMap from '../MZChallenge/containers/UsersMap';
import {Link} from 'react-router-dom';

import './UserDetail.css';

function UserDetail(props) {
  let user = props.user;
    return (
      <div className="UserInfo">
        <Link className="backLink" to="/">&lt; Back</Link>
        <div className="col-xs-7 col-sm-4 content">
          <label>Name: </label>{user.name}<br/>
          <label>Id: </label>{user.id}<br/>
          <label>username: </label>{user.username}<br/>
          <label>address: </label>
          <div className="margin-left-info">
            {user.address.street}<br/>
            {user.address.suite}<br/>
            {user.address.city}<br/>
            {user.address.zipcode}<br/>
            (coords: {user.address.geo.lat}, {user.address.geo.lng})
          </div>
          <label>Phone: </label>{user.phone}<br/>
          <label>Website: </label><a target="_blank" href={"http://"+user.website}>{user.website}</a><br/>
          <label>Company: </label>
          <div className="margin-left-info">
            {user.company.name}<br/>
            <i>{user.company.catchPhrase}</i><br/>
            {user.company.bs}
          </div>
          <label>Email: </label><a href={"mailto:"+user.email}>{user.email}</a><br/>
        </div>
        <UsersMap user={user} />
      </div>
    );
  }

export default UserDetail;
