/**
 * User Card Info
 */
import React from 'react';

function UserCard(props){
    return (
        <div>
            <h4 className="list-group-item-heading">{props.user.name} ({props.user.username})</h4>
            <p className="list-group-item-text">
            {props.user.email}<br/>
            {props.user.phone}<br/>
            {props.user.website}
            </p>
        </div>
    );
}

export default UserCard;
