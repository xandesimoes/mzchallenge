/**
 * Map with points info
 * 
 * props:
 *    [Array] initialCoord - coordinates to center the map with [lat, lng]
 *    [Int] initialZoom - value with initial zoom
 *    [Array] points - array with points info
 *    [String] className - allow to parents to add style info
 *    [Int] selectedPoint - Selected Point index to show popup
 *        point:
 *            [Number] lat - latitude of the point
 *            [Number] lng - longitude of the point
 *            [String] popupContent - html code for the popup associated to the point
 */
import React, { Component } from 'react';

import Config from '../Config';

import './MapInstance.css';

class MapInstance extends Component {

  constructor(props){
    super(props);

    //refs to DOM reference of the component - its the only reason the state its not one ancester
    this.state = {
      map: null,
      markers: []
    }
  }

  /**
   * create the map reference
   */
  componentDidMount(){
    let L = window.L;

    let mapElement = L.map('mapid').setView(this.props.initialCoord, this.props.initialZoom);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token='+Config.mapList.accessToken, {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: Config.mapList.accessToken
    }).addTo(mapElement);


    let markers = []

    //add points to map
    this.props.points.forEach( (point) => {
        let marker = L.marker([ point.lat, point.lng]).addTo(mapElement)
          .bindPopup(point.popupContent);

          markers.push(marker);
    } )

    //update state with maps and points
    this.setState( {
        markers,
        map: mapElement
    });
  }

  /**
   * Open active popup if set
   */
  openInitialPoint(){
    if(this.state.map && this.props.selectedPoint){
          this.state.markers[this.props.selectedPoint].openPopup();
    }
  }

  render() {
    this.openInitialPoint();
    return (
      <div className={'mapcontainer '+this.props.className}>
         <div id="mapid"></div>
        </div>
    );
  }
}

export default MapInstance;
