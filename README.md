#MZ Challenge

## App Requirements:
- Display a list of user cards with the info I felt relevant.
- Display all the users on a map using [leaflet](http://leafletjs.com/).
- Each user has his/her own page with the full data, and a zoomed map on their location.

## Installation
Install the dependencies and start the server.

```sh
$ npm install
$ npm start
```
